#!/bin/bash

## declare an array variable
declare -a hosts=("ciulok1" "ciulok2" "ciulok3" "ciulok4" "ciulok5")
declare -a links=("/boot/ethos-kernel" "/boot/initrd.img-ethos")
declare -a linksTargets=("/boot/vmlinuz-4.8.17-ethos49" "/boot/initrd.img-4.8.17-ethos49")
#declare -a array=("ciulok1")

# get length of an array
hostslength=${#hosts[@]}
linkslength=${#links[@]}
linksTargetslength=${#linksTargets[@]}

function deploy_minerprocess() {
    echo "deploy"
    file=minerprocess.php
    echo " $file"
    echo " to $HOST"
    destination=/opt/ethos/lib
    owner=root

    scp libs/$file $HOST:/tmp

    ssh $HOST sudo chown $owner: /tmp/$file
    ssh $HOST ls -al /tmp/$file
    ssh $HOST sudo mv $destination/$file $destination/$file.bak

    ssh $HOST sudo mv /tmp/$file $destination/
    ssh $HOST ls -al $destination/$file

    ssh $HOST putconf
}

function reboot() {
  ssh $HOST sudo reboot
}

function poweroff() {
  ssh $HOST sudo poweroff
}

function refresh_config() {
  ssh $HOST putconf
}

function clear_thermals() {
  ssh $HOST clear-thermals
}

function disable_mining() {
  ssh $HOST disallow
}

function enable_mining() {
  ssh $HOST allow
}

function update_miner() {
  ssh $HOST sudo update-miner claymore-xmr
}

function setup_timezone() {
  ssh $HOST sudo timedatectl set-timezone Europe/Berlin 
  ssh $HOST sudo timedatectl set-ntp on
}

function check_all_links() {
for (( j=1; j<${linkslength}+1; j++ ));
do
  echo $j " / " ${linkslength} " : " ${links[$j-1]}
  my_link="${links[$j-1]}"
  my_link_target="${linksTargets[$j-1]}"
  check_link
  echo "----------------------"
  echo " "
done
}

function check_link() {
  ssh -t $HOST bash -c "'
   
    if [ -L ${my_link} ] ; then
      if [ -e ${my_link} ] ; then
        echo "$HOST - ${my_link} - Good link"
      else
        echo "$HOST - ${my_link} - Broken link"
        # FIX LINK BEGIN
        echo "BEFORE FIX"
        ls -al ${my_link}
        sudo rm ${my_link}
        sudo ln -s ${my_link_target} ${my_link}
        echo "AFTER FIX"
        ls -al ${my_link}
        # FIX LINK END
      fi
    elif [ -e ${my_link} ] ; then
      echo "$HOST - ${my_link} - Not a link"
    else
      echo "$HOST - ${my_link} - Missing"
    fi
sudo ls /root
'"
}

# use for loop to read all values and indexes
for (( i=1; i<${hostslength}+1; i++ ));
do
  echo $i " / " ${hostslength} " : " ${hosts[$i-1]}
  HOST="${hosts[$i-1]}"
  #deploy_minerprocess
  #refresh_config
  #update_miner
  #check_all_links
  setup_timezone
  #reboot
  #poweroff
done
