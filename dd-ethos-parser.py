import time
from datetime import datetime


def ethosStatusParser(logger, line):

    date = time.mktime(datetime.now().timetuple())
    tags = ('ethos', 'miner', 'amd', 'claymore')
    attr_dict = {}
    hashrate = 0.0
    hashrate = float(line.split(' ')[0])
    metric_value = hashrate
    metric_name = 'miner.claymore.hashrate'
    attr_dict['tags'] = tags
    attr_dict['metric_type'] = 'counter'

    return (metric_name, date, metric_value, attr_dict)


def ethosHashParser(logger, attrs):

    date = time.mktime(datetime.now().timetuple())
    tags = ('os:ethos', 'role:miner', 'gpu:amd', 'miner:claymore')

    attr_dict = {}
    i = 0
    hashrate = 0.0
    for attr_pair in attrs.split(' '):
        attr_name = 'GPU' + str(i)
        attr_val = attr_pair.strip()
        attr_dict[attr_name] = float(attr_val)
        hashrate += float(attr_val)
        i += 1

    metric_value = hashrate
    metric_name = 'miner.claymore.hashrate'
    attr_dict['tags'] = tags
    attr_dict['metric_type'] = 'counter'

    return (metric_name, date, metric_value, attr_dict)
